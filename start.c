#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>

int fd;

void WriteFile(char *aURL, int aCOUNT, int aSUMsize, char *aMAXFileName)
{
        write(fd,"Путь к подкаталогу: ",strlen("Путь к подкаталогу: "));
        strcat(aURL,"\n");
        write(fd,aURL,strlen(aURL));

        char COUNT[200];
        sprintf(COUNT, "%d", aCOUNT);
        write(fd,"Количество файлов: ",strlen("Количество файлов: "));
        strcat(COUNT,"\n");
        write(fd,COUNT,strlen(COUNT));

        char SUMsize[200];
        sprintf(SUMsize, "%d", aSUMsize);
        write(fd,"Суммарный размер: ",strlen("Суммарный размер: "));
        strcat(SUMsize,"\n");
        write(fd,SUMsize,strlen(SUMsize));

        write(fd,"Наибольший файл: ",strlen("Наибольший файл: "));
        strcat(aMAXFileName,"\n");
        write(fd,aMAXFileName,strlen(aMAXFileName));
}

void print(char *FILEname, int FILEsize)
{
        printf(" Имя и размер файла: %s %d байт(а)\n",FILEname, FILEsize);
}

void * pDir (void *DirName)
{
        char _DirName[200];
        strcpy(_DirName, (char*)DirName);
        char Name[200];
        int SUMsize=0;
        int count=0;
        int MAXsize=0;
        char MAXFileName[200];
        strcpy(MAXFileName,"Not File");

        DIR *dir;

        struct dirent *ent;
        struct stat st;
        dir=opendir(_DirName);

        if (dir != NULL)
        {
                while ((ent = readdir(dir)) != NULL)
                {
                        strcpy(Name,"");
                        strcpy(Name,_DirName);
                        strcpy(Name,strcat(Name,"/"));
                        strcpy(Name,strcat(Name,ent->d_name));
                        stat(Name,&st);

                        if (S_ISDIR(st.st_mode)==0)
                        {
                                if ((strcmp(ent->d_name,".")!=0) && (strcmp(ent->d_name,"..")!=0))
                                {
                                        SUMsize=SUMsize+st.st_size;
                                        count++;
                                        if (MAXsize<st.st_size)
                                        {
                                                MAXsize=st.st_size;
                                                strcpy(MAXFileName,ent->d_name);
                                        }

                                }
                                print(ent->d_name,st.st_size);
                        }
                }

                printf("Путь к подкаталогу: %s\n Количество файлов: %d\n Суммарный размер: %d байт(а)\n Наибольший файл: %s\n\n",
                        _DirName, count, SUMsize, MAXFileName);
                WriteFile(_DirName, count, SUMsize, MAXFileName);
                closedir(dir);
        }

        else
        perror("Ошибка открытия каталога!!!");

        return NULL;

}

int main(int argc, char *argv[])

{
        int size=0;
        char Name[200];
        int result;
        DIR *dir;
        struct dirent *ent;
        struct stat st;
        int id=0;
        int i;

        if (argc < 1)
        {
                printf("Введены не все параметры!!!\n");
                exit(1);
        }

        fd = open(argv[2], O_WRONLY | O_CREAT);

        if (fd < 0)
        {
                printf("Ошибка открытия файла!!!\n");
                exit(1);
        }

        dir = opendir(argv[1]);

        if (dir != NULL)
        {
                while ((ent = readdir(dir)) != NULL)
                {
                        strcpy(Name,"");
                        strcpy(Name,argv[1]);
                        strcpy(Name,strcat(Name,"/"));
                        strcpy(Name,strcat(Name,ent->d_name));
                        stat(Name,&st);

                        if (S_ISDIR(st.st_mode)==0)
                        {
                                size = size + st.st_size;
                        }
                        else
                        {
                                if ((strcmp(ent->d_name,".")!=0) && (strcmp(ent->d_name,"..")!=0))
                                {
                                        pDir((void *)Name);
                                }
                        }
                }

                printf("Суммарный размер файлов в корневом каталоге %s %d байт‚\n",argv[1], size);
                closedir(dir);
        }
        else
        perror("Ошибка открытия каталога!!!");

        if (close(fd) != 0)
        {
                printf("Ошибка закрытия файла!!!\n");
                exit(1);
        }
        return 0;
}
