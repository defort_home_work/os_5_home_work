#include <time.h> 
#include <stdio.h> 
#include <math.h> 
#include <sys/time.h> 
 
int main() { 
        pid_t pid; 
        if (pid = fork()) { 
                pid = fork()
        } 
        char buffer[26]; 
        int millisec; 
        struct tm* tm_info; 
        struct timeval tv; 
 
        gettimeofday(&tv, NULL); 
 
        millisec = lrint(tv.tv_usec/1000.0); 
        if (millisec>=1000) { 
                millisec -=1000; 
                tv.tv_sec++; 
        } 
 
        tm_info = localtime(&tv.tv_sec); 
 
        strftime(buffer, 26, "%H:%M:%S", tm_info); 
        printf("%s.%03d", buffer, millisec); 
        printf(" "); 
        if (pid == 0) { 
                printf("Это дочерний процесс, его pid = %d\n", getpid()); 
                printf("Pid его родительского процесса = %d\n", getppid()); 
        } else if (pid > 0) { 
                printf("Это родительский процесс, pid = %d\n", getpid()); 
                system("ps -x"); 
        } else { 
                printf("Ошибка. Потомок не был создан"); 
        } 
}
